const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/My name is/, done);
  });

  it('accepts greetings', function(done) {
    request(app)
      .post('/greetings', { name: "Inigo Montoya" })
      .expect(/Welcome/, done);
  });
}); 
